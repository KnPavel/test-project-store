package com.store.service;

import com.store.model.Price;
import com.store.repository.PriseRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

@Service
public class PriceService {

    private PriseRepository priseRepository;

    @Autowired
    public PriceService(PriseRepository priseRepository) {
        this.priseRepository = priseRepository;
    }

    public void updatePrice(List<Price> newPrice) {
        if (newPrice == null || newPrice.isEmpty()) {
            return;
        }
        Comparator<Price> comparator = Comparator.comparing(Price::getProductCode);
        comparator = comparator.thenComparing(Price::getNumPrice);
        comparator = comparator.thenComparing(Price::getNumDepartment);
        comparator = comparator.thenComparing(Price::getBegin);
        newPrice.sort(comparator);

        List<Price> old = getAll();
        mergeOldAndNewPrices(old, newPrice);
        // TODO: to think
        clearAll();
        priseRepository.saveAll(old);
    }

    private List<Price> mergeOldAndNewPrices(List<Price> old, List<Price> young) {
        for (Price price : young) {
            merge(price, old);
        }
        return old;
    }

    private List<Price> merge(Price nNew, List<Price> list) {
        ListIterator<Price> iterator = list.listIterator();
        while (iterator.hasNext()) {
            Price old = iterator.next();
            if (!(old.getProductCode().equals(nNew.getProductCode()))
                    && old.getNumDepartment() != nNew.getNumDepartment()
                    && old.getNumPrice() != nNew.getNumPrice()) {
                continue;
            }
            if (!(old.containsDate(nNew.getBegin()) || old.containsDate(nNew.getEnd()))) {
                continue;
            }
            if (nNew.getBegin().before(old.getBegin()) || nNew.getBegin().equals(old.getBegin())) {
                if (nNew.getEnd().before(old.getEnd())) {
                    old.setBegin(nNew.getEnd());
                } else {
                    iterator.remove();
                }
            } else if (nNew.getBegin().after(old.getBegin())) {
                if (nNew.getEnd().before(old.getEnd())) {
                    Price prAf = new Price(old.getProductCode(), old.getNumPrice(), old.getNumDepartment(),
                            old.getBegin(), nNew.getBegin(), old.getValue());
                    Price prBf = new Price(old.getProductCode(), old.getNumPrice(), old.getNumDepartment(),
                            nNew.getEnd(), old.getEnd(), old.getValue());
                    iterator.remove();
                    iterator.add(prAf);
                    iterator.add(prBf);
                } else {
                    old.setEnd(nNew.getBegin());
                }
            }
            iterator.add(nNew);
        }
        return list;
    }

    public Price addNewPrice(Price price) {
        return priseRepository.save(price);
    }

    public Iterable<Price> addPrices(List<Price> prices) {
        return priseRepository.saveAll(prices);
    }

    public List<Price> getAll() {
        return priseRepository.findAll();
    }

    public void clearAll() {
        priseRepository.deleteAll();
    }

    public Price getPrice(String productCode, int numDept, int numPrice, long val) {
        return priseRepository.findByProductCodeAndNumDepartmentAndNumPriceAndValue(productCode, numDept, numPrice, val);
    }
}
