package com.store.repository;

import com.store.model.Price;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PriseRepository extends CrudRepository<Price, Long> {

    @Query("select p from Price p order by p.productCode, p.numPrice, p.productCode, p.begin")
    List<Price> findAll();

    Price findByProductCodeAndNumDepartmentAndNumPriceAndValue(String productCode, int numDepart, int numPrice, long val);
}
