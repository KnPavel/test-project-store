package com.store.model;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "price")
public class Price {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    @Column(name = "product_code")
    private String productCode;
    @Column(name = "number_price")
    private int numPrice;
    @Column(name = "number_department")
    private int numDepartment;
    @Temporal(value = TemporalType.TIMESTAMP)
    private Date begin;
    @Temporal(value = TemporalType.TIMESTAMP)
    private Date end;
    private long value;
    @Transient
    private boolean removed = false;

    public Price() {
    }

    public Price(String productCode, int numPrice, int numDepartment, Date begin, Date end, long value) {
        this.productCode = productCode;
        this.numPrice = numPrice;
        this.numDepartment = numDepartment;
        this.begin = begin;
        this.end = end;
        this.value = value;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getProductCode() {
        return productCode;
    }

    public void setProductCode(String productCode) {
        this.productCode = productCode;
    }

    public int getNumPrice() {
        return numPrice;
    }

    public void setNumPrice(int numPrice) {
        this.numPrice = numPrice;
    }

    public int getNumDepartment() {
        return numDepartment;
    }

    public void setNumDepartment(int numDepartment) {
        this.numDepartment = numDepartment;
    }

    public Date getBegin() {
        return begin;
    }

    public void setBegin(Date begin) {
        this.begin = begin;
    }

    public Date getEnd() {
        return end;
    }

    public void setEnd(Date end) {
        this.end = end;
    }

    public long getValue() {
        return value;
    }

    public void setValue(long value) {
        this.value = value;
    }

    public boolean isRemoved() {
        return removed;
    }

    public void setRemoved(boolean removed) {
        this.removed = removed;
    }

    public boolean containsDate(Date date) {
        boolean con = !(date.before(this.begin) || date.after(this.end));
        boolean eql = date.equals(this.begin) || date.equals(this.end);
        return con || eql;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Price price = (Price) o;

        if (id != price.id) {
            return false;
        }
        if (numPrice != price.numPrice) {
            return false;
        }
        if (numDepartment != price.numDepartment) {
            return false;
        }
        if (value != price.value) {
            return false;
        }
        if (productCode != null ? !productCode.equals(price.productCode) : price.productCode != null) {
            return false;
        }
        if (begin != null ? !begin.equals(price.begin) : price.begin != null) {
            return false;
        }
        return end != null ? end.equals(price.end) : price.end == null;
    }

    @Override
    public int hashCode() {
        int result = (int) (id ^ (id >>> 32));
        result = 31 * result + (productCode != null ? productCode.hashCode() : 0);
        result = 31 * result + numPrice;
        result = 31 * result + numDepartment;
        result = 31 * result + (begin != null ? begin.hashCode() : 0);
        result = 31 * result + (end != null ? end.hashCode() : 0);
        result = 31 * result + (int) (value ^ (value >>> 32));
        return result;
    }

    @Override
    public String toString() {
        return "Price{" +
                "id=" + id +
                ", productCode='" + productCode + '\'' +
                ", numPrice=" + numPrice +
                ", numDepartment=" + numDepartment +
                ", begin=" + begin +
                ", end=" + end +
                ", value=" + value +
                '}';
    }
}
