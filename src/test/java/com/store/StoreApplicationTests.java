package com.store;

import com.store.model.Price;
import com.store.service.PriceService;
import org.assertj.core.internal.Dates;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import static org.junit.Assert.assertEquals;

@RunWith(SpringRunner.class)
@SpringBootTest
public class StoreApplicationTests {

	@Autowired
	private PriceService priceService;

	private DateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy hh:mm:ss");

	@Before
	public void fillTestData() throws ParseException {
		Price price1 = new Price("122856", 1, 1,
				dateFormat.parse("10.10.2017 01:00:00"), dateFormat.parse("12.12.2017 00:00:00"), 11000);
		Price price2 = new Price("122856", 2, 1,
				dateFormat.parse("10.11.2017 00:00:00"), dateFormat.parse("25.12.2017 23:59:59"), 99000);
		Price price3 = new Price("6654", 1, 2,
				dateFormat.parse("01.12.2017 00:00:00"), dateFormat.parse("01.01.2018 23:59:59"), 5000);
		priceService.addPrices(Arrays.asList(price1, price2, price3));
	}

	private List<Price> getNewPrices() throws ParseException {
		Price price1 = new Price("122856", 1, 1,
				dateFormat.parse("15.10.2017 00:00:00"), dateFormat.parse("20.11.2017 00:00:00"), 15000);
		Price price2 = new Price("122856", 2, 1,
				dateFormat.parse("01.11.2017 00:00:00"), dateFormat.parse("01.12.2017 23:59:59"), 79000);
		Price price3 = new Price("6654", 1, 2,
				dateFormat.parse("02.12.2017 00:00:00"), dateFormat.parse("29.12.2017 23:59:59"), 3000);
		return Arrays.asList(price1, price2, price3);
	}

	@Test
	public void updatePrices() throws ParseException {
		priceService.updatePrice(getNewPrices());
		assertEquals(priceService.getAll().size(), 8);
		assertEquals(priceService.getPrice("6654", 2,1, 3000).getBegin().getTime(),
				dateFormat.parse("02.12.2017 00:00:00").getTime());
	}

}
